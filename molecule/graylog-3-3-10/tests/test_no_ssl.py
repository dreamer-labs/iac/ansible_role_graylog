import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('graylog')


def test_graylog_web(host):

    result = host.run(
        'curl http://localhost').stdout

    assert 'Graylog Web Interface' in result
